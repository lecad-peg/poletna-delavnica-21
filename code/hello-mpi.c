#include <mpi.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
  int id;
  int procesov;
  MPI_Init (&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);
  MPI_Comm_size(MPI_COMM_WORLD, &procesov);
  char meow[1024];
  gethostname(meow,1023);
    
  printf("Hello World! Moje ime je %s\n", meow);
  MPI_Finalize();
  return 0;
}