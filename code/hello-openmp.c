#include <stdio.h>

int main(int argc, char *argv[])
{
  #pragma omp omp_get_max_threads()
  #pragma omp parallel
  {
    printf("Hello World!\n");
  }

  return 0;
}