# Poletna delavnica HPC 2021 #

Kode in gradivo za delavnico HPC v okviru Poletne šole strojništva 2021

## Prevajanje in zagon kod na Raspberry Pi 4

### OpenMP C

Kode prevajamo z:

    gcc -o hello-openmp hello-openmp.c -fopenmp

in poganjamo z:

    ./hello-openmp

### OpenMP Fortran90

Kode prevajamo z:

    gfortran -o hello-openmp-f90 hello-openmp.f90 -fopenmp

in poganjamo z:

    ./hello-openmp-f90

### MPI C

Kode prevajamo z:

    mpicc -o hello-mpi hello-mpi.c

in poganjamo z:

    mpirun -np 4 ./hello-mpi
